#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

# 1. install this tool to use this script: https://github.com/elasticsearch-dump/elasticsearch-dump
# 2. get the provided ES dump and excract the archive to a directory
# 3. call this script like ./restoreESdump.sh path/to/dump/directory/

multielasticdump --debug --direction=load --input=$1  --output=http://localhost:9200 \
                --ignoreChildError=true --includeType=settings,mapping,alias,analyzer,data
