In order to run the cluster:

- Have minikube installed and working (should work with any k8s environment). Make sure to provide k8s with at least 8 GB of memory, otherwise ElasticSearch might misbehave
- Install all of these tools
  - Helm: https://helm.sh/docs/intro/install/
  - Helmfile: https://github.com/roboll/helmfile#installation
  - Helm git: https://github.com/aslafy-z/helm-git#install
  - `helm plugin install https://github.com/databus23/helm-diff`
  - k9s command line admin UI: https://k9scli.io/topics/install/
- Clone this repo and while inside the directory execute `helmfile sync`
- Fire up `k9s` and you should see services come up online. Some services will not be able to start yet, because they require a populated instance of elasticsearch
- Execute `./restoreESdump.sh <path/to/ElasticSearch/dump/directory`
- After the above completes (will probably take ~10 min), you might need to restart ff-fasttext-api (ctrl+d in k9s)
- k9s is very intuitive, so you can see what commands are available in your context in the upper part of the screen. Most important is the `<shift+f>` to do port forwarding, in order to access services in the cluster. 
- Later, if updates are made to some services, `./update-appVersions.sh && helmfile sync` should update the cluster to the latest versions.