#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

### Scrubber
head=$(git ls-remote git@gitlab.com:flaxandteal/onyx/onyx-search-scrubber.git main)
sha=${head:0:8}
echo "Setting scrubber to ${sha}"
yq -i ".image.tag = \"${sha}\" " config/search-scrubber/values.yaml

### Berlin
head=$(git ls-remote https://gitlab.com/flaxandteal/onyx/berlin-rs.git main)
sha=${head:0:8}
echo "Setting berlin to ${sha}"
yq -i ".image.tag = \"${sha}\" " config/berlin/values.yaml

### Category Server
head=$(git ls-remote https://gitlab.com/flaxandteal/onyx/ff_fasttext_poc.git main)
sha=${head:0:8}
echo "Setting ff-fasttext-api to ${sha}"
yq -i ".image.tag = \"${sha}\" " config/ff-fasttext-api/values.yaml

### Hub
head=$(git ls-remote git@gitlab.com:flaxandteal/onyx/dp-nlp-alpha-hub.git master)
sha=${head:0:8}
echo "Setting dp-nlp-alpha-hub to ${sha}"
yq -i ".image.tag = \"${sha}\" " config/dp-nlp-alpha-hub/values.yaml

### Onyx Test UI
head=$(git ls-remote git@gitlab.com:flaxandteal/onyx/onyx-test-ui.git main)
sha=${head:0:8}
echo "Setting onyx-test-ui to ${sha}"
yq -i ".image.tag = \"${sha}\" " config/onyx-test-ui/values.yaml
